
const FIRST_NAME = "BOGDAN";
const LAST_NAME = "TUDOSE";
const GRUPA = "1075";

/**
 * Make the implementation here
 */

function initCaching() {
    const cache = {};
    return {
        pageAccessCounter: (pageName = 'home') => 
        {
            pageName = pageName.toLowerCase();
            if (!cache[pageName]) 
                    cache[pageName] = 0;
            cache[pageName]++;
        },

        getCache: () => cache
    }
}


module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

